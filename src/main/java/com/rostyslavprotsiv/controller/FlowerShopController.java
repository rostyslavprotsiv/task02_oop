package com.rostyslavprotsiv.controller;

import com.rostyslavprotsiv.model.action.FlowerShopAction;
import com.rostyslavprotsiv.model.entity.AbstractFlower;
import com.rostyslavprotsiv.model.entity.FlowerShop;
import com.rostyslavprotsiv.model.creator.FlowerShopCreator;

import java.util.ArrayList;

public final class FlowerShopController {
    private static final FlowerShop SHOP = FlowerShopCreator.create();
    private static final FlowerShopController INSTANCE =
            new FlowerShopController();
    private static final FlowerShopAction ACT = new FlowerShopAction();

    private FlowerShopController() {
    }

    public static FlowerShopController getInstance() {
        return INSTANCE;
    }

    public String[][] allFlowers() {
        return ACT.getInfoAboutAllFlowers();
    }

    public String[] getBouquet(
            final ArrayList<ArrayList<String>> futureBouquet) {
        return  ACT.generateBouquet(futureBouquet, SHOP);
    }

    public ArrayList<AbstractFlower> getAllFromShop() {
        return SHOP.getFlowers();
    }
}
