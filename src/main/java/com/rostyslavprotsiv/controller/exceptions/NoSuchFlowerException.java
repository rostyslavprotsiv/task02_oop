package com.rostyslavprotsiv.controller.exceptions;

public class NoSuchFlowerException extends Exception {
    public NoSuchFlowerException() {
    }

    public NoSuchFlowerException(final String s) {
        super(s);
    }

    public NoSuchFlowerException(final String s,
                                 final Throwable throwable) {
        super(s, throwable);
    }

    public NoSuchFlowerException(final Throwable throwable) {
        super(throwable);
    }

    public NoSuchFlowerException(final String s,
                                 final Throwable throwable,
                                 final boolean b,
                                 final boolean b1) {
        super(s, throwable, b, b1);
    }
}
