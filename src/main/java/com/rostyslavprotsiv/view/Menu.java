package com.rostyslavprotsiv.view;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;
import com.rostyslavprotsiv.controller.FlowerShopController;

public final class Menu {

    private static final ArrayList<ArrayList<String>>
            ALL_FLOWERS_DESCRIPTION = new ArrayList<>();
    private static final FlowerShopController CONTROLLER =
            FlowerShopController.getInstance();
    private static final String[][] FLOWERS_INFO = CONTROLLER.allFlowers();
    private static final Menu INSTANCE = new Menu();
    private static final Scanner SCAN = new Scanner(System.in, "UTF-8");

    private Menu() {
    }

    public void show() {
        inputFlowerDescription();
        System.out.println("Do you want one more flower ?" +
                "(enter true or false)");
        if (SCAN.nextBoolean()) {
            show();
        } else {
            buyFlower();
        }
    }

    private void inputFlowerDescription() {
        ArrayList<String> flowerDescription = new ArrayList<>();
        final int threeFirstCharacteristics = 3;
        int currentFlower;
        System.out.println("Hello, this is our shop!!" + "\n"
                + "Please, choose what kind of " +
                "flower do you want ?(input number)");
        for (int i = 1; i < FLOWERS_INFO.length; i++) {
            System.out.println(i + " : "  + FLOWERS_INFO[i][0]);
        }
        currentFlower = SCAN.nextInt();
        System.out.println("Please, enter the " +
                "characteristics of your future flower");
        flowerDescription.add(FLOWERS_INFO[currentFlower][0]);
        for (int i = 0; i < threeFirstCharacteristics; i++) {
            System.out.println(FLOWERS_INFO[0][i]);
            flowerDescription.add(SCAN.next());
        }
        for (int i = 1; i < FLOWERS_INFO[currentFlower].length; i++) {
            System.out.println(FLOWERS_INFO[currentFlower][i]);
            flowerDescription.add(SCAN.next());
        }
        ALL_FLOWERS_DESCRIPTION.add(flowerDescription);
    }

    private void buyFlower() {
        String[] bouquetInfo = CONTROLLER.getBouquet(ALL_FLOWERS_DESCRIPTION);
        if (bouquetInfo == null) {
            System.out.println("Nu such bouquets in our storage." + "\n"
                    + "What we can suggest : ");
            System.out.println(CONTROLLER.getAllFromShop());
            System.out.println("Do you want to continue ? (true of false)");
            if (SCAN.nextBoolean()) {
                ALL_FLOWERS_DESCRIPTION.clear();
                show();
            } else {
                System.out.println("Bye bye!");
            }
        } else {
            //All flowers we ordered in shop
            System.out.println(ALL_FLOWERS_DESCRIPTION);
            //All flowers from shop
            System.out.println(CONTROLLER.getAllFromShop());
            //sold bouquet
            System.err.println("SOLD !!");
            System.out.println(Arrays.toString(bouquetInfo));
        }
    }

    public static  Menu getInstance() {
        return INSTANCE;
    }
}
