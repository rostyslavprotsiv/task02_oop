package com.rostyslavprotsiv;


import com.rostyslavprotsiv.view.Menu;

/**
 *
 * @version 1.0.0 30 March 2019
 * @author Rostyslav Protsiv
 * @since 2019-03-30
 */
public class Main {

    public static void main(final String[] args) {
        Menu.getInstance().show();
    }
}
