package com.rostyslavprotsiv.model.exceptions;

public class AbstractFlowerLogicalException extends Exception {
    public AbstractFlowerLogicalException() {
    }

    public AbstractFlowerLogicalException(final String s) {
        super(s);
    }

    public AbstractFlowerLogicalException(final String s,
                                          final Throwable throwable) {
        super(s, throwable);
    }

    public AbstractFlowerLogicalException(final Throwable throwable) {
        super(throwable);
    }

    public AbstractFlowerLogicalException(final String s,
                                          final Throwable throwable,
                                          final boolean b,
                                          final boolean b1) {
        super(s, throwable, b, b1);
    }
}
