package com.rostyslavprotsiv.model.exceptions;

public class LilyOfValleyLogicalException extends Exception {

    public LilyOfValleyLogicalException() {
    }

    public LilyOfValleyLogicalException(final String s) {
        super(s);
    }

    public LilyOfValleyLogicalException(final String s,
                                        final Throwable throwable) {
        super(s, throwable);
    }

    public LilyOfValleyLogicalException(final Throwable throwable) {
        super(throwable);
    }

    public LilyOfValleyLogicalException(final String s,
                                        final Throwable throwable,
                                        final boolean b,
                                        final boolean b1) {
        super(s, throwable, b, b1);
    }
}
