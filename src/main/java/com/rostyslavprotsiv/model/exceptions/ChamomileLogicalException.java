package com.rostyslavprotsiv.model.exceptions;

public class ChamomileLogicalException extends Exception {
    public ChamomileLogicalException() {
    }

    public ChamomileLogicalException(final String s) {
        super(s);
    }

    public ChamomileLogicalException(final String s,
                                     final Throwable throwable) {
        super(s, throwable);
    }

    public ChamomileLogicalException(final Throwable throwable) {
        super(throwable);
    }

    public ChamomileLogicalException(final String s,
                                     final Throwable throwable,
                                     final boolean b,
                                     final boolean b1) {
        super(s, throwable, b, b1);
    }
}
