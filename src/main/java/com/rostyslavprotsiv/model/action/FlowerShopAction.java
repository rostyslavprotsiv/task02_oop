package com.rostyslavprotsiv.model.action;

import com.rostyslavprotsiv.controller.exceptions.NoSuchFlowerException;
import com.rostyslavprotsiv.model.entity.AbstractFlower;
import com.rostyslavprotsiv.model.entity.FlowerShop;
import com.rostyslavprotsiv.model.entity.Rose;
import com.rostyslavprotsiv.model.entity.Chamomile;
import com.rostyslavprotsiv.model.entity.Clove;
import com.rostyslavprotsiv.model.entity.LilyOfValley;
import com.rostyslavprotsiv.model.exceptions.AbstractFlowerLogicalException;
import com.rostyslavprotsiv.model.exceptions.ChamomileLogicalException;
import com.rostyslavprotsiv.model.exceptions.LilyOfValleyLogicalException;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class FlowerShopAction {

    public final void createSortedBouquet(final ArrayList<AbstractFlower> flowers) {
        Comparator<AbstractFlower> comp = new Comparator<AbstractFlower>() {
            public int compare(final AbstractFlower one, final AbstractFlower two) {
                return Float.compare(two.getPrice(), one.getPrice());
            }
        // public boolean equals(Object ob) { /* realization */ }
        };
        Collections.sort(flowers, comp);
    }

    public final String[][] getInfoAboutAllFlowers() {
        // more code with using libraries of files,that must read info about classes
        String info[][] = {{"price", "length", "color"},
                {"Chamomile", "hasSmell", "amountOfLeafs"},
                {"Clove", "isBushy"},
                {"LilyOfValley", "amountOfBells", "DiameterOfBells"},
                {"Rose", "hasSpikes"}};
        return info;
    }

    public final String[] generateBouquet(final ArrayList<ArrayList<String>> futureBouquet,
                                    final FlowerShop shop) {
        ArrayList<AbstractFlower> createdBouquet = createBouquet(futureBouquet);
        if (isBouquetInStorage(createdBouquet, shop)) {
            shop.getFlowers().removeAll(createdBouquet);
            return decodeBouquet(createdBouquet);
        } else {
            return null;
        }
    }

    private boolean isBouquetInStorage(
            final ArrayList<AbstractFlower> createdBouquet,
            final FlowerShop shop) {
        ArrayList<AbstractFlower> allFlowers = shop.getFlowers();
        final int initialSizeOfBouquet = createdBouquet.size();
        createdBouquet.retainAll(allFlowers);
        if (createdBouquet.size() == initialSizeOfBouquet) {
            return true;
        }
        return false;
    }

    private String[] decodeBouquet(
            final ArrayList<AbstractFlower> createdBouquet) {
        final int sizeOfCreatedBouquet = createdBouquet.size();
        String[] decodedBouquet = new String[sizeOfCreatedBouquet];
        for (int i = 0; i < sizeOfCreatedBouquet; i++) {
            decodedBouquet[i] = createdBouquet.get(i).toString();
        }
        return decodedBouquet;
    }

    private ArrayList<AbstractFlower> createBouquet(
            final ArrayList<ArrayList<String>> futureBouquet) {
        ArrayList<AbstractFlower> allInputtedFlowers = new ArrayList<>();
            for (int i = 0; i < futureBouquet.size(); i++) {
                allInputtedFlowers.add(createObject(futureBouquet.get(i)));
            }
        createSortedBouquet(allInputtedFlowers);
        return allInputtedFlowers;
    }

    private AbstractFlower createObject(final ArrayList<String> flower) {
        AbstractFlower findingFlower = null;
        try {
            switch (flower.get(0)) {
                case "Chamomile" :
                        findingFlower = new Chamomile(
                                Float.valueOf(flower.get(1)),
                                Float.valueOf(flower.get(2)),
                                flower.get(3), Boolean.valueOf(flower.get(4)),
                                flower.get(5));
                    break;
                case "Clove" :
                        findingFlower = new Clove(Float.valueOf(flower.get(1)),
                                Float.valueOf(flower.get(2)),
                                flower.get(3), Boolean.valueOf(flower.get(4)));
                    break;
                case "LilyOfValley" :
                        findingFlower = new LilyOfValley(
                                Float.valueOf(flower.get(1)),
                                Float.valueOf(flower.get(2)),
                                flower.get(3), Integer.parseInt(flower.get(4)),
                                Float.valueOf(flower.get(5)));
                    break;
                case "Rose" :
                        findingFlower = new Rose(Float.valueOf(flower.get(1)),
                                Float.valueOf(flower.get(2)),
                                flower.get(3), Boolean.valueOf(flower.get(4)));
                    break;
                default :
                    throw new NoSuchFlowerException();
            }
        } catch (AbstractFlowerLogicalException | ChamomileLogicalException
                | LilyOfValleyLogicalException | NoSuchFlowerException exep) {
            exep.printStackTrace();
            System.err.println("Error with creating object of flower");
        }
        return findingFlower;
    }
}
