package com.rostyslavprotsiv.model.creator;

import com.rostyslavprotsiv.model.entity.AbstractFlower;
import com.rostyslavprotsiv.model.entity.FlowerShop;
import com.rostyslavprotsiv.model.entity.Rose;
import com.rostyslavprotsiv.model.entity.Chamomile;
import com.rostyslavprotsiv.model.entity.Clove;
import com.rostyslavprotsiv.model.entity.LilyOfValley;
import com.rostyslavprotsiv.model.exceptions.AbstractFlowerLogicalException;
import com.rostyslavprotsiv.model.exceptions.ChamomileLogicalException;
import com.rostyslavprotsiv.model.exceptions.LilyOfValleyLogicalException;

import java.util.ArrayList;

public class FlowerShopCreator {

    private FlowerShopCreator() {
    }

    public static FlowerShop create() {
        //connect to db
        ArrayList<AbstractFlower> allFlowers = new ArrayList<>();
        try {
            allFlowers.add(new Rose(10, 10, "BLUE", true));
            allFlowers.add(new Rose(10, 26, "RED", true));
            allFlowers.add(new Chamomile(20, 23, "green", true, "Rare"));
            allFlowers.add(new LilyOfValley(13, 35, "yellow", 30, 200));
            allFlowers.add(new Clove(56, 47, "red", true));
        } catch (AbstractFlowerLogicalException
                | ChamomileLogicalException | LilyOfValleyLogicalException exep) {
            exep.printStackTrace();
            System.err.println("Error with filling shop storage");
        }
        return new FlowerShop(allFlowers);
    }
}
