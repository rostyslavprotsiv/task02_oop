package com.rostyslavprotsiv.model.entity;

import java.util.ArrayList;
import java.util.Objects;

public class FlowerShop {
    private ArrayList<AbstractFlower> flowers;

    public FlowerShop() {
    }

    public FlowerShop(final ArrayList<AbstractFlower> flowers) {
        this.flowers = flowers;
    }

    public ArrayList<AbstractFlower> getFlowers() {
        return flowers;
    }

    public void setFlowers(final ArrayList<AbstractFlower> flowers) {
        this.flowers = flowers;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        FlowerShop that = (FlowerShop) o;
        return Objects.equals(flowers, that.flowers);
    }

    @Override
    public int hashCode() {
        return Objects.hash(flowers);
    }

    @Override
    public String toString() {
        return "FlowerShop{" +
                "flowers=" + flowers +
                '}';
    }
}
