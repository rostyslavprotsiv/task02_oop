package com.rostyslavprotsiv.model.entity;

import com.rostyslavprotsiv.model.exceptions.AbstractFlowerLogicalException;

import java.util.Objects;

public class Rose extends AbstractFlower {
    protected boolean hasSpikes;

    public Rose() {
    }

    public Rose(final boolean hasSpikes) {
        this.hasSpikes = hasSpikes;
    }

    public Rose(final float price, final float length,
                final String color, final boolean hasSpikes)
            throws AbstractFlowerLogicalException {
        super(price, length, color);
        this.hasSpikes = hasSpikes;
    }

    public boolean isHasSpikes() {
        return hasSpikes;
    }

    public void setHasSpikes(final boolean hasSpikes) {
        this.hasSpikes = hasSpikes;
    }

    @Override
    public String findOutLifetime() {
        //Find out time of life from the internet sources
        return "1.2 year";
    }
    @Override
    public String findDillers() {
        //Find out time of life from the internet sources
        return "Ashot Sargysyan";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        Rose rose = (Rose) o;
        return hasSpikes == rose.hasSpikes;
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), hasSpikes);
    }

    @Override
    public String toString() {
        return "Rose{" +
                "hasSpikes=" + hasSpikes +
                ", price=" + price +
                ", length=" + length +
                ", color=" + color +
                '}';
    }
}
