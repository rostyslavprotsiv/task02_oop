package com.rostyslavprotsiv.model.entity;

import com.rostyslavprotsiv.model.exceptions.AbstractFlowerLogicalException;

import java.util.Objects;

public class Clove extends  AbstractFlower {
    protected boolean isBushy;

    public Clove() {
    }

    public Clove(final boolean isBushy) {
        this.isBushy = isBushy;
    }

    public Clove(final float price, final float length,
                 final String color, final boolean isBushy)
            throws AbstractFlowerLogicalException {
        super(price, length, color);
        this.isBushy = isBushy;
    }

    public boolean isBushy() {
        return isBushy;
    }

    public void setBushy(final boolean bushy) {
        isBushy = bushy;
    }

    @Override
    public  String findOutLifetime() {
        //Find out time of life from the internet sources
        return "1.8 year";
    }

    @Override
    public  String findDillers() {
        //Find out time of life from the internet sources
        return "Karen Gurdash";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        Clove clove = (Clove) o;
        return isBushy == clove.isBushy;
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), isBushy);
    }

    @Override
    public String toString() {
        return "Clove{" +
                "isBushy=" + isBushy +
                ", price=" + price +
                ", length=" + length +
                ", color=" + color +
                '}';
    }
}
