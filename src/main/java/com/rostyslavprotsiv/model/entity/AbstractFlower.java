package com.rostyslavprotsiv.model.entity;

import java.util.Objects;
import com.rostyslavprotsiv.model.exceptions.AbstractFlowerLogicalException;

public abstract class AbstractFlower {
    protected float price;
    protected float length;
    protected Color color;
    private enum Color {
        BLUE, RED, GREEN, WHITE, YELLOW
    }

    public AbstractFlower() {
    }

    public AbstractFlower(final float price,
                          final float length,
                          final String color)
            throws AbstractFlowerLogicalException {
        if (checkColor(color) && checkPrice(price) && checkLength(length)) {
            this.price = price;
            this.length = length;
            this.color = Color.valueOf(color.toUpperCase());
        } else {
            throw new AbstractFlowerLogicalException();
        }
    }

    public AbstractFlower(final float price,
                          final String color) throws AbstractFlowerLogicalException {
        if (checkColor(color) && checkPrice(price)) {
            this.price = price;
            this.color = Color.valueOf(color.toUpperCase());
        } else {
            throw new AbstractFlowerLogicalException();
        }
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(final float price) {
        this.price = price;
    }

    public float getLength() {
        return length;
    }

    public void setLength(final float length) {
        this.length = length;
    }

    public Color getColor() {
        return color;
    }

    public void setColor(final String color)
            throws AbstractFlowerLogicalException {
        if (checkColor(color)) {
            this.color = Color.valueOf(color.toUpperCase());
        } else {
            throw new AbstractFlowerLogicalException();
        }
    }

    private boolean checkPrice(final float price) {
        final int maxValue = 9999;
        return price > 0 && price < maxValue;
    }

    private boolean checkLength(final float length) {
        final int maxValue = 9999;
        return length > 0 && length < maxValue;
    }

    private boolean checkColor(final String color) {
        final Color[] allColors = Color.values();
        final int amountOfColors = Color.values().length;
        return checkStringForElem(allColors, amountOfColors, color);
    }

    protected <T> boolean checkStringForElem(final T[] allElements,
                                             final int amountOfElements,
                                             final String elem) {
        boolean elementExists = false;
        for (int i = 0; i < amountOfElements; i++) {
            if (allElements[i].toString().equals(elem.toUpperCase())) {
                elementExists = true;
            }
        }
        return elementExists;
    }

    public abstract String findOutLifetime();
    public abstract String findDillers();

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AbstractFlower that = (AbstractFlower) o;
        return Float.compare(that.price, price) == 0 &&
                Float.compare(that.length, length) == 0 &&
                color == that.color;
    }

    @Override
    public int hashCode() {
        return Objects.hash(price, length, color);
    }

    @Override
    public String toString() {
        return "AbstractFlower{" +
                "price=" + price +
                ", length=" + length +
                ", color=" + color +
                '}';
    }
}
