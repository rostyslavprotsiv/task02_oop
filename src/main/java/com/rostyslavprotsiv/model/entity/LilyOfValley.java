package com.rostyslavprotsiv.model.entity;

import com.rostyslavprotsiv.model.exceptions.AbstractFlowerLogicalException;
import com.rostyslavprotsiv.model.exceptions.LilyOfValleyLogicalException;

import java.util.Objects;

public class LilyOfValley extends AbstractFlower {
    protected int amountOfBells;
    protected float diameterOfBells;

    public LilyOfValley() {
    }

    public LilyOfValley(final int amountOfBells, final float diameterOfBells)
            throws LilyOfValleyLogicalException {
            if (checkAmountOfBells(amountOfBells)
                    && checkDiameterOfBells(diameterOfBells)) {
                this.amountOfBells = amountOfBells;
            this.diameterOfBells = diameterOfBells;
        } else {
            throw new LilyOfValleyLogicalException();
        }
    }

    public LilyOfValley(final float price, final float length, String color,
                        final int amountOfBells, final float diameterOfBells)
            throws AbstractFlowerLogicalException,
            LilyOfValleyLogicalException {
        super(price, length, color);
        if (checkAmountOfBells(amountOfBells)
                && checkDiameterOfBells(diameterOfBells)) {
            this.amountOfBells = amountOfBells;
            this.diameterOfBells = diameterOfBells;
        } else {
            throw new LilyOfValleyLogicalException();
        }
    }


    public int getAmountOfBells() {
        return amountOfBells;
    }

    public void setAmountOfBells(final int amountOfBells) throws LilyOfValleyLogicalException {
        if (checkAmountOfBells(amountOfBells)) {
            this.amountOfBells = amountOfBells;
        } else {
            throw new LilyOfValleyLogicalException();
        }
    }

    public float getDiameterOfBells() {
        return diameterOfBells;
    }

    public void setDiameterOfBells(final float diameterOfBells)
            throws LilyOfValleyLogicalException {
        if (checkDiameterOfBells(diameterOfBells)) {
            this.diameterOfBells = diameterOfBells;
        } else {
            throw new LilyOfValleyLogicalException();
        }
    }

    private boolean checkAmountOfBells(final int amountOfBells) {
        final int maxAmount = 200;
        return amountOfBells > 0 && amountOfBells <= maxAmount;
    }

    private boolean checkDiameterOfBells(final float diameterOfBells) {
        final int maxDiameter = 2000;
        final int minDiameter = 188;
        return diameterOfBells > minDiameter && diameterOfBells < maxDiameter;
    }

    @Override
    public  String findOutLifetime() {
        //Find out time of life from the internet sources
        return "2.3 year";
    }
    @Override
    public  String findDillers() {
        //Find out time of life from the internet sources
        return "Yaroslav Perepichka";
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        LilyOfValley that = (LilyOfValley) o;
        return amountOfBells == that.amountOfBells &&
                Float.compare(that.diameterOfBells, diameterOfBells) == 0;
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), amountOfBells, diameterOfBells);
    }

    @Override
    public String toString() {
        return "LilyOfValley{" +
                "amountOfBells=" + amountOfBells +
                ", diameterOfBells=" + diameterOfBells +
                ", price=" + price +
                ", length=" + length +
                ", color=" + color +
                '}';
    }
}
