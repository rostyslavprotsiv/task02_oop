package com.rostyslavprotsiv.model.entity;

import com.rostyslavprotsiv.model.exceptions.AbstractFlowerLogicalException;
import com.rostyslavprotsiv.model.exceptions.ChamomileLogicalException;

import java.util.Objects;

public class Chamomile extends AbstractFlower {
    protected boolean hasSmell;
    protected LeafsDensity amountOfLeafs;
    private enum LeafsDensity {
        RARE, MEDIUM, BUSHY
    }

    public Chamomile() {
    }

    public Chamomile(final boolean hasSmell,
                     final String amountOfLeafs)
            throws ChamomileLogicalException {
        if (checkAmountOfLeafs(amountOfLeafs)) {
            this.hasSmell = hasSmell;
            this.amountOfLeafs = LeafsDensity.valueOf(
                    amountOfLeafs.toUpperCase());
        } else {
            throw new ChamomileLogicalException();
        }
    }

    public Chamomile(final float price,
                     final float length,
                     final String color,
                     final boolean hasSmell,
                     final String amountOfLeafs)
            throws AbstractFlowerLogicalException, ChamomileLogicalException {
        super(price, length, color);
        if (checkAmountOfLeafs(amountOfLeafs)) {
            this.hasSmell = hasSmell;
            this.amountOfLeafs = LeafsDensity.valueOf(
                    amountOfLeafs.toUpperCase());
        } else {
            throw new ChamomileLogicalException();
        }
    }

    public boolean isHasSmell() {
        return hasSmell;
    }

    public void setHasSmell(final boolean hasSmell) {
        this.hasSmell = hasSmell;
    }

    public LeafsDensity getAmountOfLeafs() {
        return amountOfLeafs;
    }

    public void setAmountOfLeafs(final String amountOfLeafs)
            throws ChamomileLogicalException {
        if (checkAmountOfLeafs(amountOfLeafs)) {
            this.amountOfLeafs = LeafsDensity.valueOf(
                    amountOfLeafs.toUpperCase());
        } else {
            throw new ChamomileLogicalException();
        }
    }

    @Override
    public  String findOutLifetime() {
        //Find out time of life from the internet sources
        return "2.2 year";
    }
    @Override
    public  String findDillers() {
        //Find out time of life from the internet sources
        return "Gazar Sarhashnet";
    }

    private boolean checkAmountOfLeafs(final String amountOfLeafs) {
        final LeafsDensity[] allDensities = LeafsDensity.values();
        final int amountOfDensities = LeafsDensity.values().length;
        return checkStringForElem(allDensities, amountOfDensities, amountOfLeafs);
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        Chamomile chamomile = (Chamomile) o;
        return hasSmell == chamomile.hasSmell &&
                amountOfLeafs == chamomile.amountOfLeafs;
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), hasSmell, amountOfLeafs);
    }

    @Override
    public String toString() {
        return "Chamomile{" +
                "hasSmell=" + hasSmell +
                ", amountOfLeafs=" + amountOfLeafs +
                ", price=" + price +
                ", length=" + length +
                ", color=" + color +
                '}';
    }
}

